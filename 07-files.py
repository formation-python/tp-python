'''

Moyenne des notes

Le fichier notes.txt contient les notes obtenues par des étudiants pour le cours de Python. Chaque ligne du fichier ne contient qu'une note.
Créez un script Python qui lit chaque ligne de ce fichier, extrait les notes sous forme de float et les stocke dans une liste.

Terminez le script en calculant et affichant la moyenne des notes avec deux décimales.

Le script réécrira ensuite les notes dans le fichier notes2.txt avec une note par ligne suivie de « recalé » si la note
 est inférieure à 10 et « admis » si la note est supérieure ou égale à 10. Toutes les notes seront écrites avec une décimale.

'''
import urllib
import math as mathematics
import  functions

print(mathematics.factorial(4))

print(functions.calc_puissance(2,3))

notes = []

with open("test.txt","r", encoding='utf-8') as filein:
    lines = filein.readlines()
    for line in lines:
        notes.append(line.rstrip())
        print(notes)









#modes : r => read , a => ajouter à la fin du fichier, w => ecrire / ecraser, r+ => lire et ecrire, b => ouvrir un fichier en mode binaire, x pour création d'un fichier,
# t pour lire le fichier en mode text





