from copy import deepcopy

courses_names_list = ['maths', 'Python',
                      'IA', 'Django', 'Angular', 'Rest', 'Git']
print(courses_names_list)
courses_durations = [22, 12, 14, 23, 20, 1, 22]
print(courses_durations)

print(courses_names_list[0])
print(courses_names_list[-1])

full_list = courses_names_list + courses_durations

many_courses  = courses_names_list*3
print(many_courses)

many_courses.append('IA')
print(many_courses)

print(many_courses)


# predire les résultat des instructions suivantes

""" courses_names_list[1]
courses_names_list[0]
courses_names_list[7]
courses_durations[-1] """

# tranches
#retourner la tranche qui va de 1 à 2 inculs
print(courses_names_list[1:3])
print(courses_names_list[3:])
print(courses_names_list[:4])


a = list(range(1, 11))
print(a)

print(a[1:8:2])

print(len(a))


groupe1 = ['Albert','Alex']
groupe2 = ['François','jean']
groupe3 = ['Aline','Amelie']

salle_classe = [groupe1,groupe2,groupe3]

print(salle_classe)

print(salle_classe[2])


a=[22, 12, 14, 23, 20, 1, 22]
a.insert(3,-20)
a.remove(-20)
print(a)

a = [1,2,3]
#b=a[:]
b = a.copy()
print(a)
print(b)
b[0]=-3
print(b)
print(a)


# Exercices

""" 1. Jours de la semaine

Constituez une liste semaine contenant les 7 jours de la semaine.
À partir de cette liste, comment récupérez-vous seulement les 5 premiers jours de la semaine d'une part, et ceux 
du week-end d'autre part ? Utilisez pour cela l'indiçage.
Cherchez un autre moyen pour arriver au même résultat(en utilisant un autre indiçage).
Trouvez deux manières pour accéder au dernier jour de la semaine.
Inversez les jours de la semaine en une commande.

2 Saisons

Créez 4 listes hiver, printemps, ete et automne contenant les mois correspondants à ces saisons. Créez ensuite une
liste saisons contenant les listes hiver, printemps, ete et automne. Prévoyez ce que renvoient les instructions suivantes,
puis vérifiez-le dans l'interpréteur:

saisons[2]
saisons[1][0]
saisons[1:2]
saisons[:][1]. Comment expliquez-vous ce dernier résultat ?


3 Table de multiplication par 9

Affichez la table de multiplication par 9 en une seule commande avec les instructions range() et list().

4 Nombres pairs

Répondez à la question suivante en une seule commande. Combien y a-t-il de nombres pairs dans l'intervalle[2, 10000] inclus ? """

semaine = ['lundi', 'mardi', 'mercredi','jeudi','vendredi','samedi','dimanche']
print(semaine[:5])
print(semaine[5:])
print(semaine[-2:])
print(semaine[:-2])
print(semaine[-1])
print(semaine[6])
semaine.reverse()
print(semaine.reverse())

#saison

été = ['juin','juillet','aout']
automne = ['septembre','octobre','novembre']
hiver = ['decembre','janvier','fevrier']
printemps = ['mars','avril','mai']

saisons = [été,automne,hiver,printemps]
print(saisons)

print(saisons[:][1])

print(list(range(0,82,9)))
print(len(list(range(2,10001,2))))
