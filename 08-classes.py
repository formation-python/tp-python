'''class Car:
    def __init__(self, color="Noire", brand="Inconnue"):
        self.color = color
        self.brand = brand

    def run(self):
        print(f"La voiture {self.brand} est en marche")


print(Car)

my_volvo = Car()
print(my_volvo)

#####

my_car = Car("Red","Mercedes")
print(my_car.color)
print(my_car.brand)
my_car.run()
'''
from math import *

class Car:

    def __init__(self,color="Noire", brand="Inconne", name="Unknown"):
        self.color=color
        self.brand=brand
        self.name = name


    def demarrer(self):
        print(f"La voiture {self.name} de marque {self.brand} et de couleur {self.color} est en marche")


ma_voiture= Car(color="Rouge",brand="Volvo",name="magnifique")
ma_voiture.demarrer()


class Rectangle:


    def __init__(self, longueur=0.0, largeur=0.0, couleur="blanc"):
        """Initialisation d'un objet.

        Définition des attributs avec des valeurs par défaut.
        """
        self.longueur = longueur
        self.largeur = largeur
        self.couleur = couleur

    def calcule_surface(self):
        """Méthode qui calcule la surface."""
        return self.longueur * self.largeur

    def change_carre(self, cote):
        """Méthode qui transforme un rectangle en carré."""
        self.longueur = cote
        self.largeur = cote

    def calcule_perim(self):
        return (self.longueur + self.largeur) * 2


    @property
    def longueur(self):
        print('getting the longueur')
        return self._longueur

    @longueur.setter
    def longueur(self, value):
        print(f'setting the longueur {value}')
        if value < 0:
            print('la longeur est négative')
        else:
            self._longueur = value

    @property
    def largeur(self):
        print('getting the largeur')
        return self._largeur

    @largeur.setter
    def largeur(self, value):
        print(f'setting the largeur : {value}')
        if value<0:
            print('la largeur est négative')
        else:
            self._largeur = value


class ARectangle:


    def __init__(self, longueur=0.0, largeur=0.0, couleur="blanc"):
        """Initialisation d'un objet.

        Définition des attributs avec des valeurs par défaut.
        """
        self.longueur = longueur
        self.largeur = largeur
        self.couleur = couleur

    def calcule_surface(self):
        """Méthode qui calcule la surface."""
        return self.longueur * self.largeur

    def change_carre(self, cote):
        """Méthode qui transforme un rectangle en carré."""
        self.longueur = cote
        self.largeur = cote

    def calcule_perim(self):
        return (self.longueur + self.largeur) * 2


    def get_longueur(self):
        print('getting the longueur')
        return self._longueur

    def set_longueur(self, value):
        print(f'setting the longueur {value}')
        if value < 0:
            print('la longeur est négative')
        else:
            self._longueur = value

    def get_largeur(self):
        print('getting the largeur')
        return self._largeur

    def set_largeur(self, value):
        print(f'setting the largeur : {value}')
        if value<0:
            print('la largeur est négative')
        else:
            self._largeur = value

    largeur = property(fget=get_largeur, fset=set_largeur)
    longueur = property(fget=get_longueur, fset=set_longueur)


'''
rectangle = Rectangle(longueur=10, largeur=20)

rectangle.longueur = -50
print(rectangle.longueur)
rectangle.longueur = 5
print(rectangle.longueur)
'''


#decorateurs

def affichage():
    print("affichage simple")


affichage()

print("")

def ajout_decoration(func):
    def inner(*args,**kwargs):
        print("****************")
        return func(*args,**kwargs)
    return inner


decoration = ajout_decoration(affichage)
decoration()



@ajout_decoration
def un_autre_message():
    print("debut de l'affichage")
    print(1+1)
    print("Un autre affichage")
    print("fin de l'affichage")




un_autre_message()

def test_division(func):
    def inner(a,b):
        print(f"On divise {a} par {b}")
        if b == 0:
            print("c'est l'infini")
            return
        return func(a,b)
    return inner


@test_division
def division(a,b):
    return a/b



def ajouter_affichage(func):
    def inner(*args,**kwargs):
        print('****** appel de la fonction')
        return func(*args,**kwargs)
    return inner

@ajouter_affichage
@ajout_decoration
def concat_message(a,b,c,fin=""):
    message = f"{a} et {b} et {c} et pour finir {fin}"
    return message





division(2,0)



print(concat_message("test", "hey", "salut", fin="c'est la fin"))


#fin_decorateurs


'''
construire un décorateur qui permet de verifier si tous les arguments d'une fonction sont positifs
construire un décorateur qui permet de verifier si tous les arguments sont differents de 0

ajouter ces deux décorateurs à ces deux fonctions 

def calcul_surface(a,b):
    return a*b

def calcul_somme(a,b,c,d,old_sum=0)
    return a+b+c+d+old_sum

'''

def mult_self(func):
    def inner():
        x = func()
        return x*x
    return inner


def mult_2(func):
    def inner():
        x = func()
        return 2*x
    return inner


@mult_self
@mult_2
def numero():
    return 10



print(numero())


def test_positif(func):
    def inner(*args,**kwargs):

        for arg in args:
            if arg<0:
                print('Il y a une valeur négative')
        for kwarg,value in kwargs.items():
            if value < 0:
                print('Il y a une valeur négative')

        return func(*args,**kwargs)
    return inner

def test_nulle(func):
    def inner(*args,**kwargs):

        for i in range(len(args)):
            if args[i] == 0:
                print('Il y a une valeur nulle')
                return 'Impossible à calculer avec une valeur nulle'
        for kwarg,value in kwargs.items():
            print(value)
            if value == 0:
                print('Il y a une valeur nulle')
                return 'Impossible à calculer avec une valeur nulle'

        return func(*args,**kwargs)
    return inner

@test_nulle
@test_positif
def calcul_somme(a,b,c,d,old_sum=0):
    return a+b+c+d+old_sum

@test_nulle
@test_positif
def calcul_produit(a,b):
    return a*b


print(calcul_somme(1,2,3,d=4,old_sum=10))
print(calcul_produit(2,3))
'''
    Complétez le programme principal pour que le script :

crée une instance rectangle de la classe Rectangle ;
affiche les attributs d'instance largeur, longueur et couleur ;
calcule et affiche la surface de rectangle ;

change le rectangle en carré de 30 m de côté ;
calcule et affiche la surface de ce carré ;

crée une autre instance rectangle2 aux dimensions et à la couleur que vous souhaitez (soyez créatif !)
et qui affiche les attributs et la surface de ce nouveau rectangle.

Classe Rectangle améliorée

Entraînez-vous avec la classe Rectangle. Créez la méthode calcule_perimetre() qui calcule le périmètre d'un objet rectangle.
Testez sur un exemple simple (largeur = 10 m, longueur = 20 m).





Classe Atome

Créez une nouvelle classe Atome avec les attributs x, y, z (qui contiennent les coordonnées atomiques) et la méthode calcul_distance()
 qui calcule la distance entre deux atomes. Testez cette classe sur plusieurs exemples.
    

'''


class Atome:
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def calcul_distance(self,x,y,z):
        return (((self.x-x)**2) +((self.y-y)**2) +(self.z-z)**2)**(1/2)


atome1 = Atome(x=2,y=0,z=6)


