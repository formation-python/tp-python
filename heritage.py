class Mere:
    def bonjour(self):
        return "Vous avez le bonjour de la classe mère !"


class Fille(Mere):
    def salut(self):
        return "Un salut de la classe fille !"


fille = Fille()
print(fille.salut())
print(fille.bonjour())


class Transport:
    def __init__(self, capacity=None, brand=None):
        self.capacity = capacity
        self.brand = brand

    def affiche_msg(self, transport_type, msg):

        return ("Instance {}\ncapacité: {}, marque: {},\n"
                "message: {}\n"
                .format(transport_type, self.capacity, self.brand,msg))





class Car(Transport):
    def __init__(self, capacity=None, brand=None, doors=None, carburant='essance'):
        self.doors = doors
        self.carburant = carburant
        Transport.__init__(self, capacity=capacity, brand=brand)

    def msg(self):
        return self.affiche_msg(transport_type="Voiture", msg="ça roule")


class Plane(Transport):
    def __init__(self, capacity=None, brand=None, max_speed=None):
        self.max_speed = max_speed
        Transport.__init__(self, capacity=capacity, brand=brand)

    def msg(self):
        return self.affiche_msg(transport_type="Avion", msg="ça vole")




ma_voiture = Car(capacity=5, brand='Mercedes',doors=5)
print(ma_voiture.msg())



my_plane = Plane(capacity=367, brand="Airbus",max_speed=899)
print(my_plane.msg())


