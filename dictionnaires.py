



personne = {

    'information_perso': {
        'nom' : 'De Oliveira',
        'prenom' : 'Milho'
    },
    'classes' : ['Java','python'],
    'matricule' : 177260

}

other_infos = {
    'email':"email@gmail.com"
}

personne['information_perso'].update(other_infos)

print(personne)

print(personne.items())

for key, value in personne.items():
    print(key + '=>'+ str(value))



personne['matricule']=0

print(personne)

personne.update({'info':personne['information_perso']})
personne.pop('information_perso')


if 'info2' in personne:
    print('Oui ça existe')
    print(personne['info'])



print(list(personne.keys()))

personne.clear()
personne.update({'clé':2})

print(personne)

impairs=[1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]
print(impairs)
impairs_tuple=(1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21)
print(impairs_tuple)
impairs[1]=-90
print(impairs)

print(impairs_tuple[2:5])

tuple2=tuple(impairs)
print(tuple2)

lists = [1,2,3,3]
tuples = (1,2,3,3)
s = {1,2,3,3}

print(lists)
print(tuples)
print(s)