x = 22
print(x)

y = 3.14
print(y)

message = '''
ceci est un message
Pi'''
print(message)

# Opérations
# Les symboles +, -, *, /, **, // et % sont appelés opérateurs, car ils réalisent des opérations sur les variables

x = 22 + 4
print('Addition')
print(x)

x = 34 - 4
print('soustraction')
print(x)

x = 67 * 2
print('multiplication')
print(x)

x = 2 ** 2
print('puissance')
print(x)

x = 5 / 2
print('division')
print(x)

x = 5 // 2
print('quotient')
print(x)

x = 5 % 2
print('reste de la division')
print(x)


# Opérations sur les strings

message = 'Hello'
print(message)

mon_premier_message = 'Hello2'

message = message + ' Bonjour '
print(message)

message = message * 2
print(message)

# Conversion des types

x = 3
print(x)
print(str(x))

message_num = '23'
print(message_num)
print(type(message_num))
print(int(message_num))
print(type(int(message_num)))

# exercices : Quels sont résultats des instructions suivantes :

v1 = (1+2)**3
v2 = "Da" * 4
v3 = "Da" + 3
v5 = ("Pa"+"La") * 2
v6 = ("Da"*4) / 2
v7 = 5 / 2
v8 = 5 // 2
v9 = 5 % 2
v10 = str(4) * int("3")
v11 = int("3") + float("3.2")
v12 = str(3) * float("3.2")
v13 = str(3/4) * 2

