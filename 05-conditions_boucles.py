'''
mot_de_passe = '1234'
mdp = input('quel est le mot de passe ?')

if mdp == mot_de_passe:
    print('Mot de passe validé')
else:
    print('Mot de passe erroné')

print('au revoir')





# if elif


user_input = input('choisissez un nombre parmis la liste [1, 4, 12] : ')

if user_input == '1':
    print("Vous avez gagné un cahier")
elif user_input == '4':
    print('Vous avez gagné un voyage à la lune')
elif user_input == '12':
    print('Vous avez gagné une limonade')
else:
    print('Vous avez perdu')


# and, or


user_input = int(input('Quel est votre age : '))

if user_input < 0 or user_input > 125:
    print('Bon, voyons...')
if user_input > 1 and user_input < 18:
    print('Revenez dans quelques années')
if user_input == 18:
    print('félicitations ! vous pouvez entrer dans le monde des adultes')
if user_input > 18 and user_input <= 125:
    print("Vous savez ce que c'est déja :)")

'''

été = ['juin','juillet','aout']
automne = ['septembre','octobre','novembre']
hiver = ['decembre','janvier','fevrier']
printemps = ['mars','avril','mai']

saisons = [été,automne,hiver,printemps]
'''
for sai in saisons:
    print(sai)


for i in range(0,10,2):
    print(i)

for i in range(len(saisons)):
    print(saisons[i])
'''


'''

#while
i = 0
while i < len(saisons):
    print(saisons[i])
    i=i+1
    print(i)






guess = 0
while guess != 9:
    guess = int(input('devinez un nombre'))
else:
    print('Bravo vous avez trouvé le nombre')



semaine = ['lundi', 'mardi', 'mercredi','jeudi','vendredi','samedi','dimanche']

'''



#boucle
'''
for jour in semaine:
    #condition?
    if jour=='lundi' or jour =='mardi' or jour =='mercredi' or jour =='jeudi':
        print(f'{jour} : Jour du travail')
    elif jour =='vendredi':
        print(f'{jour} :chouette')
    else:
        print(f'{jour} :week-end')
'''
'''
for jour in semaine:
    if semaine[:4].count(jour) > 0:
        print(f'{jour} : Jour du travail')
    elif semaine[5:].count(jour)>0:
        print(f'{jour} :week-end')
    else:
        print(f'{jour} :chouette')

n=2.2334344
print('{:.2f}'.format(n))
print(round(n,2))
'''



'''6.7 Exercices


6.7.1 Jours de la semaine

Constituez une liste semaine contenant le nom des sept jours de la semaine.

En utilisant une boucle, écrivez chaque jour de la semaine ainsi que les messages suivants :

Au travail s'il s'agit du lundi au jeudi ;
Chouette c'est vendredi s'il s'agit du vendredi ;
Repos ce week-end s'il s'agit du samedi ou du dimanche.
Ces messages ne sont que des suggestions, vous pouvez laisser libre cours à votre imagination.






6.7.5 Notes et mention d'un étudiant

Voici les notes d'un étudiant : 14, 9, 13, 15 et 12. Créez un script qui affiche la note maximum 
(utilisez la fonction max()), la note minimum (utilisez la fonction min()) et qui calcule la moyenne.

Affichez la valeur de la moyenne avec deux décimales.
Affichez aussi la mention obtenue sachant que la mention est « passable » si la moyenne est entre 10 inclus et 12 exclus,
« assez bien » entre 12 inclus et 14 exclus et « bien » au-delà de 14.






5.4.1 Boucles de base

Soit la liste ['vache', 'souris', 'levure', 'bacterie']. Affichez l'ensemble des éléments de cette liste (un élément par ligne) de trois manières différentes (deux avec for et une avec while).
'''

elementsList = ['vache', 'souris', 'levure', 'bacterie']

for element in elementsList:
    print(element)


for i in range(0,len(elementsList)):
    print(elementsList[i])

i=0
while i < len(elementsList):
    print(elementsList[i])
    i=i+1


'''
5.4.2 Boucle et jours de la semaine

Constituez une liste semaine contenant les 7 jours de la semaine.

Écrivez une série d'instructions affichant les jours de la semaine (en utilisant une boucle for), ainsi qu'une autre série d'instructions affichant les jours du week-end (en utilisant une boucle while).

5.4.3 Nombres de 1 à 10 sur une ligne

Avec une boucle, affichez les nombres de 1 à 10 sur une seule ligne.


5.4.4 Nombres pairs et impairs

Soit impairs la liste de nombres [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]. Écrivez un programme qui, à partir de la liste impairs, construit une liste pairs dans laquelle tous les éléments de impairs sont incrémentés de 1.

5.4.5 Calcul de la moyenne

Voici les notes d'un étudiant [14, 9, 6, 8, 12]. Calculez la moyenne de ces notes. Utilisez l'écriture formatée pour afficher la valeur de la moyenne avec deux décimales.

5.4.6 Produit de nombres consécutifs

Avez les fonctions list() et range(), créez la liste entiers contenant les nombres entiers pairs de 2 à 20 inclus.

Calculez ensuite le produit des nombres consécutifs deux à deux de entiers en utilisant une boucle. Exemple pour les premières itérations :

'''

message=''

for i in range(1, 11):
    print(i, end=' ')

print('')

message=''

for i in range(1,11):
    message=message+ str(i)

print(message)


impairs=[1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]
pairs=[]

for element in impairs:
    pairs.append(element+1)
print(pairs)

for i in range(len(impairs)):
    impairs[i]=impairs[i]+1

print(impairs)


notes =[14, 9, 6, 8, 12]
somme = 0

for n in notes:
    somme = somme +n

moyenne = somme/len(notes)
print('{:.2f}'.format(moyenne))
print(round(moyenne,2))



list_pairs = list(range(2,21,2))
print(list_pairs)


for i in range(len(list_pairs)-1):
    print(list_pairs[i]*list_pairs[i+1])



for i in range(11):
    print('*'*(i+1))


#boucle sur le nombre de lignes
for i in range(11):
    #boucler sur le nombre d'etoiles
    for j in range(i+1):
        print('*', end='')
    print()

#boucle sur le nombre de lignes
for i in range(11):
    #boucler sur le nombre d'etoiles
    etoiles=''
    for j in range(i+1):
        etoiles+='*'
    print(etoiles)



for i in range(10,0,-1):
    print('*'*i)




for i in range(11):
    print(' '*(11-i-1)+'*'*i)


for i in range(11):
    print(' ' * (11 - i - 1) + '*' * (2*i+1))



'''

8
24
48
[...]


5.4.7 Triangle

Créez un script qui dessine un triangle comme celui-ci :

*
**
***
****
*****
******
*******
********
*********
**********
5.4.8 Triangle inversé

Créez un script qui dessine un triangle comme celui-ci :


**********
*********
********
*******
******
*****
****
***
**
*


5.4.9 Triangle gauche

Créez un script qui dessine un triangle comme celui-ci :

         *
        **
       ***
      ****
     *****
    ******
   *******
  ********
 *********
**********


5.4.10 Pyramide

Créez un script pyra.py qui dessine une pyramide comme celle-ci :


         *
        ***
       *****
      *******
     *********
    ***********
   *************
  ***************
 *****************
*******************



'''


