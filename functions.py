

def pyramid(x):
    for i in range(x):
        print(' ' * (x - i - 1) + '*' * (2 * i + 1))


pyramid(20)
pyramid(10)
pyramid(5)



def fois(x,y):
    return x*y


print(fois(2,2))
print(fois('test ',2))


def calc_puissance(x,y=2):
    return x**y


print(calc_puissance(y=3.2,x=2))
print(calc_puissance(x=4,y=3))


for i in range(21):
    print(f'2^ {i} = {calc_puissance(2,i)}')


def print_loop(*arguments,**kwargs):
    for argument in arguments:
        print(argument)
    print(kwargs['name'])
    print(kwargs['x'])




print_loop(x=2,y=4,name='Youssef')




def listes(num):
    for i in range(num):
        puissance = 2**i
        print(f'2^{i} = {puissance}')




listes(21)










'''

I.
Créez une fonction calc_puissance(x, y) qui renvoie
x^y
 en utilisant l'opérateur **

II.
Dans le programme principal, calculez et affichez à l'écran 2^i
 avec i variant de 0 à 20 inclus. On souhaite que le résultat soit présenté avec le formatage suivant :
2^ 0 =       1
2^ 1 =       2
2^ 2 =       4
[...]
2^20 = 1048576

'''